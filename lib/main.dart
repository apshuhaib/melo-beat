import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:melo_beat_app/controllers/favourites_controller.dart';
import 'package:melo_beat_app/models/favourites_model.dart';
import 'package:melo_beat_app/screens/screen_bottom_nav.dart';
import 'package:melo_beat_app/screens/screen_home.dart';
import 'package:melo_beat_app/screens/screen_splash.dart';
import 'package:hive_flutter/hive_flutter.dart';

void main() async {
  // WidgetsFlutterBinding.ensureInitialized();
  // await Hive.initFlutter();
  // Hive.registerAdapter<Favourite>(FavouriteAdapter());

  // Get.put(FavouritesController());

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Melo beat',
      theme: ThemeData(
        appBarTheme: const AppBarTheme(
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
      ),
      home: SplashScreen(),
    );
  }
}
