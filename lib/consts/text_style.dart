import 'package:flutter/material.dart';
import 'package:melo_beat_app/consts/colors.dart';

ourStyle({double? size = 14, color = whiteColor}) {
  return TextStyle(
    fontSize: size,
    color: color,
  );
}
