import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:melo_beat_app/models/favourites_model.dart';

class FavouritesController extends GetxController {
  final Box<Favourite> _favouritesBox = Hive.box<Favourite>('favourites');
  RxList<Favourite> favourites = <Favourite>[].obs;

  @override
  void onInit() {
    favourites.assignAll(_favouritesBox.values.cast<Favourite>().toList());
    super.onInit();
  }

  void addFavouriteSong(Favourite song) {
    _favouritesBox.add(song);
    favourites.add(song);
  }

  void removeFavouriteSong(Favourite song) {
    _favouritesBox.delete(song);
    favourites.remove(song);
  }
}
