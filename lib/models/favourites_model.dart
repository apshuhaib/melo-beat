import 'package:hive/hive.dart';
part 'favourites_model.g.dart';

@HiveType(typeId: 1)
class Favourite {
  @HiveField(0)
  late String title;
  @HiveField(1)
  late String artist;

  Favourite({required this.title, required this.artist});
}
