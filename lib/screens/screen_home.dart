import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:melo_beat_app/consts/colors.dart';
import 'package:melo_beat_app/consts/text_style.dart';
import 'package:melo_beat_app/controllers/player_controller.dart';
import 'package:melo_beat_app/screens/screen_player.dart';
import 'package:on_audio_query/on_audio_query.dart';

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    var controller = Get.put(PlayerController());

    return Scaffold(
      backgroundColor: bgDarkColor,
      drawer: Drawer(
        backgroundColor: bgDarkColor,
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: ListView(children: [
            ListTile(
              leading: const Icon(
                Icons.notifications,
                color: Colors.white,
              ),
              title: Text(
                'Notifications',
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
              onTap: () {
                // Do something when Item 1 is clicked
              },
            ),
            ListTile(
              leading: Icon(
                Icons.favorite,
                color: Colors.white,
              ),
              title: Text(
                'Liked Songs',
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
              onTap: () {
                // Do something when Item 1 is clicked
              },
            ),
            ListTile(
              leading: Icon(
                Icons.language,
                color: Colors.white,
              ),
              title: Text(
                'Language',
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
              onTap: () {
                // Do something when Item 1 is clicked
              },
            ),
            ListTile(
              leading: Icon(
                Icons.contact_page,
                color: Colors.white,
              ),
              title: Text(
                'Contact Us',
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
              onTap: () {
                // Do something when Item 1 is clicked
              },
            ),
            ListTile(
              leading: Icon(
                Icons.lightbulb,
                color: Colors.white,
              ),
              title: Text(
                'FAQ',
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
              onTap: () {
                // Do something when Item 1 is clicked
              },
            ),
            ListTile(
              leading: Icon(
                Icons.settings,
                color: Colors.white,
              ),
              title: Text(
                'Settings',
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
              onTap: () {
                // Do something when Item 1 is clicked
              },
            ),
          ]),
        ),
      ),
      appBar: AppBar(
        actions: [IconButton(onPressed: () {}, icon: Icon(Icons.search))],
        backgroundColor: bgDarkColor,
        title: Text(
          'Melo beat',
          style: ourStyle(
            size: 18,
            color: whiteColor,
          ),
        ),
      ),
      body: FutureBuilder<List<SongModel>>(
        future: controller.audioQuery.querySongs(
            ignoreCase: true,
            sortType: null,
            orderType: OrderType.ASC_OR_SMALLER,
            uriType: UriType.EXTERNAL),
        builder: (BuildContext context, snapshot) {
          if (snapshot.data == null) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (snapshot.data!.isEmpty) {
            return Center(
              child: Text(
                'No songs found',
                style: ourStyle(size: 20),
              ),
            );
          } else {
            print(snapshot.data);
            return Padding(
              padding: EdgeInsets.all(10),
              child: ListView.builder(
                physics: BouncingScrollPhysics(),
                itemCount: snapshot.data!.length,
                itemBuilder: (context, index) {
                  return Container(
                    margin: EdgeInsets.only(bottom: 4),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Obx(
                      () => ListTile(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12)),
                        tileColor: bgColor,
                        leading: QueryArtworkWidget(
                          id: snapshot.data![index].id,
                          type: ArtworkType.AUDIO,
                          nullArtworkWidget: Icon(
                            Icons.music_note,
                            color: whiteColor,
                          ),
                        ),
                        title: Text(
                          snapshot.data![index].displayNameWOExt,
                          style: ourStyle(size: 15, color: whiteColor),
                        ),
                        subtitle: Text(
                          '${snapshot.data![index].artist}',
                          style: ourStyle(size: 15, color: whiteColor),
                        ),
                        trailing: controller.playIndex.value == index &&
                                controller.isPlaying.value
                            ? Icon(
                                Icons.play_arrow,
                                color: whiteColor,
                              )
                            : null,
                        onTap: () {
                          Get.to(
                              () => Player(
                                    data: snapshot.data!,
                                  ),
                              transition: Transition.downToUp);
                          controller.playSong(snapshot.data![index].uri, index);
                        },
                      ),
                    ),
                  );
                },
              ),
            );
          }
        },
      ),
    );
  }
}
