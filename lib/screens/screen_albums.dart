import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:melo_beat_app/consts/colors.dart';
import 'package:melo_beat_app/consts/text_style.dart';

class Albums extends StatelessWidget {
  const Albums({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgDarkColor,
      appBar: AppBar(
        title: Text(
          'My Albums',
          style: ourStyle(
            size: 18,
            color: whiteColor,
          ),
        ),
      ),
      body: SafeArea(child: Center(child: Text('Albums'))),
    );
  }
}
