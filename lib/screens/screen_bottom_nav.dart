import 'package:flutter/material.dart';
import 'package:melo_beat_app/consts/colors.dart';
import 'package:melo_beat_app/consts/text_style.dart';
import 'package:melo_beat_app/screens/screen_albums.dart';
import 'package:melo_beat_app/screens/screen_favourites.dart';
import 'package:melo_beat_app/screens/screen_home.dart';

class BottomNavigationPage extends StatefulWidget {
  const BottomNavigationPage({super.key});

  @override
  State<BottomNavigationPage> createState() => _BottomNavigationPageState();
}

class _BottomNavigationPageState extends State<BottomNavigationPage> {
  int _selectedIndex = 0;
  List<Widget> _screens = [Home(), Favourites(), Albums()];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _screens[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        iconSize: 25,
        backgroundColor: bgDarkColor,
        unselectedItemColor: whiteColor,
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
          BottomNavigationBarItem(
              icon: Icon(Icons.favorite_outline), label: 'Favourites'),
          BottomNavigationBarItem(
              icon: Icon(Icons.library_music_outlined), label: 'Play List'),
        ],
      ),
    );
  }
}
