import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:melo_beat_app/consts/colors.dart';
import 'package:melo_beat_app/consts/text_style.dart';
import 'package:melo_beat_app/controllers/favourites_controller.dart';
import 'package:melo_beat_app/models/favourites_model.dart';

class Favourites extends StatelessWidget {
  const Favourites({super.key});

  @override
  Widget build(BuildContext context) {
    final favouritesController = Get.find<FavouritesController>();

    return Scaffold(
        backgroundColor: bgDarkColor,
        appBar: AppBar(
          title: Text(
            'Liked Songs',
            style: ourStyle(
              size: 18,
              color: whiteColor,
            ),
          ),
        ),
        body: Obx(() => ListView.builder(
            itemCount: favouritesController.favourites.length,
            itemBuilder: (context, index) {
              final Favourite song = favouritesController.favourites[index];
              return ListTile(
                title: Text(song.title),
              );
            })));
  }
}
