import 'package:flutter/material.dart';
import 'package:melo_beat_app/consts/colors.dart';
import 'package:melo_beat_app/screens/screen_bottom_nav.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    navigateToHome();
  }

  Future navigateToHome() async {
    await Future.delayed(Duration(seconds: 1));

    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => BottomNavigationPage()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgDarkColor,
      body: SafeArea(
          child: Center(
        child: Image(
          image: AssetImage('assets/images/logo.jpeg'),
          height: 150,
          width: 150,
        ),
      )),
    );
  }
}
